CREATE TABLE `db_config::$DB_TABLE_ITEMS` (
        `id` INT(11) NOT NULL,
        `category` VARCHAR(150) NOT NULL,
		`item_name` VARCHAR(200) NOT NULL,
		`owner` VARCHAR(50) NOT NULL,
		`species` VARCHAR(1000) NOT NULL,
		`create_date` DATE NOT NULL,
        
    )
CREATE TABLE `db_config::$DB_TABLE_CATEGORY` (
        `cat_name` VARCHAR(200) NOT NULL,
		`owner` VARCHAR(50) NOT NULL,
		`species` VARCHAR(1000) NOT NULL,
		`create_date` DATE NOT NULL,
        
    )	
	
CREATE TABLE `db_config::$DB_TABLE_ORDERS` (
        `item_name` VARCHAR(200) NOT NULL,
		`number` VARCHAR(50) NOT NULL,
		`name` VARCHAR(1000) NOT NULL,
		`mail` VARCHAR(1000) NOT NULL,
		`create_date` DATE NOT NULL,
    )		

	