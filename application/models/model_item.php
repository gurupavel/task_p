<?php

/**
 * Created by PhpStorm.
 * User: kobap
 * Date: 04.07.2016
 * Time: 14:03
 */
class Model_Item extends Model
{
    public function get_data()
    {
        $mysqli = mysqli_connect(db_config::$DB_HOST, db_config::$DB_USER,db_config::$DB_PASS, db_config::$DB_NAME);

        /* connection check */
        if ($mysqli->connect_errno) {
            printf("Connection failed %s\n", $mysqli->connect_error);
            exit();
        }

        $query = "SELECT * FROM ". db_config::$DB_TABLE_ITEMS. ";";
        $result = $mysqli->query($query);
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
        mysqli_close($mysqli);
        return $data;

    }

    public function get_sort_by_cat()
    {
        $mysqli = mysqli_connect(db_config::$DB_HOST, db_config::$DB_USER,db_config::$DB_PASS, db_config::$DB_NAME);

        /* connection check */
        if ($mysqli->connect_errno) {
            printf("Connection failed %s\n", $mysqli->connect_error);
            exit();
        }

        $query = "SELECT * FROM ". db_config::$DB_TABLE_ITEMS. " WHERE category = '".  $_POST['cat_name'] ."' ;";
       // echo $query;
        $result = $mysqli->query($query);
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
        mysqli_close($mysqli);
        return $data;

    }
}

?>