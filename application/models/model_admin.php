<?php

class model_admin extends Model
{
        function get_data(){
            $data = NULL;
            $mysqli = mysqli_connect(db_config::$DB_HOST, db_config::$DB_USER,db_config::$DB_PASS, db_config::$DB_NAME);

            /* connection check */
            if ($mysqli->connect_errno) {
                printf("Connection failed %s\n", $mysqli->connect_error);
                exit();
            }

            $query = "SELECT * FROM ". db_config::$DB_TABLE_ORDERS. ";";
            //echo $query;
            $result = $mysqli->query($query);
            if ($result == TRUE){

                while ($row = $result->fetch_assoc()) {
                    $data[] = $row;
                }
           // echo $data; test point
		mysqli_close($mysqli);
            return $data;
}
        function preinstall()
{

            $mysqli = mysqli_connect(db_config::$DB_HOST, db_config::$DB_USER,db_config::$DB_PASS, db_config::$DB_NAME);

            /* connection check */
            if ($mysqli->connect_errno) {
                printf("Connection failed %s\n", $mysqli->connect_error);
                exit();
            }

         $query_cat = "CREATE TABLE  IF NOT EXISTS ".db_config::$DB_TABLE_CATEGORY."(
         cat_name  VARCHAR(200) NOT NULL,
		 owner  VARCHAR(50) NOT NULL,
		 species  VARCHAR(1000) NOT NULL,
		 create_date  VARCHAR(20) NOT NULL);";

         $query_items ="CREATE TABLE  IF NOT EXISTS ".db_config::$DB_TABLE_ITEMS."(
         id  INT(11) NOT NULL,
         category  VARCHAR(150) NOT NULL,
		 item_name  VARCHAR(200) NOT NULL,
		 owner  VARCHAR(50) NOT NULL,
		 species  VARCHAR(1000) NOT NULL,
		 create_date  VARCHAR(20) NOT NULL);";
         $query_orders="CREATE TABLE  IF NOT EXISTS ".db_config::$DB_TABLE_ORDERS."(
         item_name  VARCHAR(200) NOT NULL,
		 number  VARCHAR(50) NOT NULL,
		 name  VARCHAR(1000) NOT NULL,
		 mail  VARCHAR(1000) NOT NULL,
		 create_date  VARCHAR(20) NOT NULL);";

            $result = $mysqli->query($query_cat);
            $result = $mysqli->query($query_items);
            $result = $mysqli->query($query_orders);
            mysqli_close($mysqli);
            $install_result = "Creating done, next step is filling demo data";
            return $install_result;
        }


    function demo_fill()
    {
        $mysqli = mysqli_connect(db_config::$DB_HOST, db_config::$DB_USER,db_config::$DB_PASS, db_config::$DB_NAME);

        /* connection check */
        if ($mysqli->connect_errno) {
            printf("Connection failed %s\n", $mysqli->connect_error);
            exit();
        }
        echo $query_fill_item = "
        INSERT INTO ".db_config::$DB_TABLE_ITEMS." (id, category, item_name, owner, species, create_date) VALUES 
        (1, 'phones', 'Factory Unlocked Apple iPhone 5S A1533 16GB 4G LTE Touch ID Mobile Smartphone', 'Kobap', 'Internet Browsing, MMS (Multimedia Messaging), MP3 Player, Sat Nav, Touch Screen, Camera, Colour Screen, Email, Fingerprint Sensor, FM Radio This is a AAA+ Stock item, meaning unit is in good cosmetic condition and NO minor scrapes or scratches, just like a 100% new one!\n\nThis item that has been professionally restored to working order, No factory warranty, include100% new charger, USB Cable & Headphone.\n\nThat means it will work with a valid SIM card from any GSM Network worldwide. And we will give you the best gift for free!! ”', '1999-03-30'),
(2, 'watches', 'TAG HEUER FORMULA 1 CHRONOGRAPH MEN''S WATCH', 'Rolep', 'Setting the highest standards for sports inspired watch design, this TAG Heuer Formula 1 chronograph men''s watch captures the excitement of world-class motor racing. This bold design is fastened with a black, perforated rubber strap and features a 43mm stainless steel case with a fixed tachymeter bezel. Sapphire crystal glass that is scratch-resistant protects the black dial that''s brought to life with baton hour markers and Arabic numeral at 12 o''clock, date window at 4 o''clock and three chronograph sub dials. Water resistant to 200m, this striking watch is powered by a Swiss made TAG Heuer quartz movement. Lend a masculine, yet elegant edge to your look with this excellent watch, perfectly reflecting TAG Heuer''s passion for sport and their immaculate watch engineering. CAZ1010.FT8024\n\n', '2016-07-06'),
(4, 'phones', 'Samsung Galaxy J3 (2016)FOLLOW\n', 'Kozel', 'The Samsung Galaxy J3 (2016) features a 5-inch Super AMOLED screen carrying a 720 x 1280 (HD) resolution. Powering the device is the Snapdragon 410 SoC, which contains a quad-core 1.2GHz CPU and the Adreno 306 GPU. 1.5GB of RAM is inside, along with 8GB of internal storage. A 128GB capacity microSD slot is accessible for those requiring additional memory. The back of the phone features a 8MP camera, while the front-facing selfie snapper weighs in at 5MP. A 2600mAh battery keeps the generators humming, and Android 5.1 is pre-installed.', '2016-07-06'),
(3, 'Clothes', 'Fuckin PANTS', 'Kobap', 'Damm it feels good to have this shit', '2016-07-06'),
(5, 'phones', 'Samsung Convoy 4', 'Kobap', 'The Samsung Convoy 4 is almost the same as its predecessor, except the added water-resistance. This feature phone includes features like dual-core processor, 256 MB RAM, memory card slot, military-grade durability and push-to-talk.', '2016-07-06');

        ";

        echo $query_fill_cat ="
        INSERT INTO ". db_config::$DB_TABLE_CATEGORY." (cat_name, owner, species, create_date) VALUES 
        ( 'phones' ,  'admin' ,  'New phones, smartpho' ,  '2016-07-06' ),
        ( 'watches' ,  'admin' ,  'Clocks, smart watche' ,  '2016-07-06' ),
        ( 'Clothes' ,  'admin' ,  'Pants, Shirts, watch' ,  '2016-07-06' );
     ";

        $result = $mysqli->query($query_fill_item);
        $result = $mysqli->query($query_fill_cat);
        //$data = $result->fetch_all(MYSQLI_ASSOC);
        mysqli_close($mysqli);
        $install_result = "Demo data filled, enjoy!";
        return $install_result;

    }


}
}
