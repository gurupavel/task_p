<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title>Main page</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body >

<div class="container">


	<nav role="navigation" class="navbar navbar-default">

		<!-- default menu -->
		<div id="navbarCollapse" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="/<?php echo(project_config::$PROJECT_ROOT); ?>/main">Home</a></li>
				<li><a href="/<?php echo(project_config::$PROJECT_ROOT); ?>/item">Items</a></li>
				<li><a href="/<?php echo(project_config::$PROJECT_ROOT); ?>/category">Categories</a></li>
				<li><a href="/<?php echo(project_config::$PROJECT_ROOT); ?>/admin">Admin tools</a></li>
			</ul>
		</div>
	</nav>

	<?php include 'application/views/'.$content_view; ?>


	

</div>



</body>
</html>