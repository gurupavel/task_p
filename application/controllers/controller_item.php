<?php
/**
 * Created by PhpStorm.
 * User: kobap
 * Date: 04.07.2016
 * Time: 13:36
 */
class Controller_Item extends Controller
{

    function __construct()
    {
        $this->model = new Model_Item();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->get_data();
        $this->view->generate('item_view.php', 'template_view.php', $data);
    }
    
    function action_sort()
    {
        $data = $this->model->get_sort_by_cat();
        $this->view->generate('item_view.php', 'template_view.php', $data);
    }
}
?>