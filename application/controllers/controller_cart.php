<?php

/**
 * Created by PhpStorm.
 * User: kobap
 * Date: 09.07.2016
 * Time: 18:13
 */
class Controller_cart extends Controller
{

    function __construct()
    {
        $this->model = new Model_Cart();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->get_data();
        $this->view->generate('cart_view.php', 'template_view.php', $data);
    }
    function action_buy()
    {
        $data = $this->model->make_order();
        $this->view->generate('main_view.php', 'template_view.php', $data);
    }
    
    
}