<?php

/**
 * Created by PhpStorm.
 * User: kobap
 * Date: 11.07.2016
 * Time: 1:58
 */
class controller_admin extends Controller
{

       function __construct()
        {
            $this->model = new Model_Admin();
            $this->view = new View();
        }

        function action_index()
        {
            $data = $this->model->get_data();
            $this->view->generate('admin_view.php', 'template_view.php', $data);
        }

        function action_preinstall()
        {
            $data = $this->model->preinstall();
            $this->view->generate('admin_view.php', 'template_view.php', $data);
        }

    function action_demo_fill()
    {
        $data = $this->model->demo_fill();
        $this->view->generate('admin_view.php', 'template_view.php', $data);
    }
        
    }
    
    
    
